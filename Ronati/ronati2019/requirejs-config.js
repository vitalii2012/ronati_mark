var config = {
    map: {
        '*': {
            'aboutUsForm': 'js/contact-us-form',
            'cookieBanner': 'js/cookie-banner',
            'intlTelInput': 'js/libs/intlTelInput.min',
            'invertorSidebar': 'js/invertor-sidebar-class-switcher',
            'filterableSelect': 'js/libs/fSelect',
            'homeFormValidate': 'js/home-page-form-validate',
            'buyerSettingModal': 'js/buyer-setting-modal',
            'tagManager': 'js/libs/tagmanager.min',
            'signUpPasswordValidation': 'js/sign-up-password-validate',
            'typeHead': 'js/libs/bootstrap3-typeahead.min',
            "getMarketplaceForm": "js/marketplace-form-builder",
            "getDefaultMarketplaceForm": "js/marketplace-defaul-form-builder",
            "stickyKit": "js/stickyKit/sticky-kit.min",
            "stickyBlockPosition": "js/inventory-sticky",
            "select2": "js/libs/select2.min",
            "multiselect": "js/multiselect",
            'tagEditorCaret': 'js/libs/jquery.caret.min',
            'tagEditor': 'js/libs/jquery.tag-editor',
        }
    },
    "shim": {
        "tagManager": ["jquery"],
        "typeHead": ["jquery"],
        "tagEditor": ["tagEditorCaret"]
    }
};
