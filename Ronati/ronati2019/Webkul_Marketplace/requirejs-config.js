var config = {
  map: {
    '*': {
      'FilterArrowPosition': 'Webkul_Marketplace/js/filter-arrow-position-changer',
      'ProductsPagesScript': 'Webkul_Marketplace/js/produts-page-script',
      'ProductListSetDeleted': 'Webkul_Marketplace/js/delete-product-from-list',
      'ProductSetDeleted': 'Webkul_Marketplace/js/delete-product',
      'customValidations': 'Webkul_Marketplace/js/custom-validations',
      'ProductListSetOnHold': 'Webkul_Marketplace/js/put-on-hold-product-from-list',
      'deactivateProduct': 'Webkul_Marketplace/js/deactivate-product',
      'activateProduct': 'Webkul_Marketplace/js/activate-product',
      'productListPage' : 'Webkul_Marketplace/js/pages/product-list-page',
      'activeListPage' : 'Webkul_Marketplace/js/pages/active-list-page',
      'draftListPage' : 'Webkul_Marketplace/js/pages/draft-list-page',
      'onHoldListPage' : 'Webkul_Marketplace/js/pages/on-hold-list-page',
      'addProductPage' : 'Webkul_Marketplace/js/pages/add-product-page',
      'allRequestPage' : 'Webkul_Marketplace/js/pages/become-seller-page',
      'becomeSellerPage' : 'Webkul_Marketplace/js/pages/become-seller-page',
      'businessProfilePage' : 'Webkul_Marketplace/js/pages/business-profile-page',
      'marketPlaceSettingsPage' : 'Webkul_Marketplace/js/pages/marketplace-setting-page',
      'sellerChannel' : 'Webkul_Marketplace/js/pages/seller-channel-page',
      'signupPage' : 'Webkul_Marketplace/js/pages/signup-pages',
      'uncategoryProductPage' : 'Webkul_Marketplace/js/uncategory-product-list',
      'ProductSetOnHold': 'Webkul_Marketplace/js/put-on-hold-product',
      'customValidatePhone': 'Webkul_Marketplace/js/phone-validate',
      'removeFromMarketplace': 'Webkul_Marketplace/js/remove-from-mp',
      "skuGenerator": "Webkul_Marketplace/js/random-sku-generator",
      "tagEditorWidget": "Webkul_Marketplace/js/tag-editor",
      "allowSaveAsDraft": "Webkul_Marketplace/js/save-as-draft-button"
    }
  }
};
