define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('ronati.skuGenerator', {

        options: {
            skuLength: ''
        },

        _create: function () {
            this._bind();
        },

        _bind: function () {
            this._on({
                click: '_getSku'
            });
        },

        _getSku: function () {
            let result = '',
                characters = 'abcdefghijklmnopqrstuvwxyz',
                skuLenght = this.options.skuLength,
                charactersLength = characters.length;
            for (var i = 0; i < skuLenght; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            $('#sku').val(result);
        }

    });

    return $.ronati.skuGenerator;
});



