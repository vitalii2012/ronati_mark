define([
    'jquery',
    'mage/mage'
], function ($) {

    return function () {

        $('.js-tab-nav a').click(function () {
            var targetItem = $(this).attr('data-id');
            $('.js-tab-nav a').removeClass('active');
            $(this).addClass('active');
            $('#account-tabs .account-tabs').removeClass('active');
            $('#account-tabs').find('#' + targetItem).addClass('active');
        });

        //change password

        jQuery('#update-password').click(function () {
            if (!jQuery('#change_pass').valid()) {
                return false;
            } else {
                changePasswordProcess();
            }
        });

        //change language

        jQuery('#update-language').click(function () {
            if (!jQuery('#language_form').valid()) {
                return false;
            } else {
                changeLanguageProcess();
            }
        });

        //change measurement

        jQuery('#update-measurement').click(function () {
            if (!jQuery('#measurement_form').valid()) {
                return false;
            } else {
                changeMeasurementProcess();
            }
        });

        //change currency

        jQuery('#update-currency').click(function () {

            if (!jQuery('#currency_form').valid()) {
                return false;
            } else {
                changeCurrencyProcess();
            }
        });

        var origMeasurement = jQuery('#select-measurement').find(":selected").val();
        jQuery('#select-measurement').change(function () {
            if (origMeasurement != jQuery('#select-measurement').find(":selected").val()) {
                jQuery('#update-measurements').prop("disabled", false);
            } else {
                jQuery('#update-measurements').prop('checked', false);
                jQuery('#update-measurements').prop("disabled", true);
            }
        });

    }
});


