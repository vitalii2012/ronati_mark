define([
    'jquery'
], function ($) {
    return function () {
        $('#remove_from_mp').on('click', function () {
            var mpArr = [];
            var arrResult = [];
            var flag = $('.delete-product');
            flag.removeClass('has_available_items');
            $('.channel-list__item').removeClass('is_available');
            $('#remove_item').css('display', 'block');

            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    var item = $(this);
                    var mpParreentItem = item.parents('.product-list__row').find('.channels_wrapper');
                    var mpList = mpParreentItem.find('input');
                    var result = mpList.val();
                    if (result !== undefined) {
                        mpArr.push(result);
                    }
                }
            });

            mpArr.forEach(function (a) {
                arrResult = arrResult.concat(a.toString().split(','));
            });

            var unicArr = arrayUnique(arrResult);

            if(unicArr.length !== 0){
                flag.addClass('has_available_items');
                unicArr.forEach(function (e) {
                    $('.marketplace_id_' + e).addClass('is_available');
                })
            }

        });

        function arrayUnique(arr) {
            return arr.filter((e, i, a) => a.indexOf(e) == i)
        }
    }
});

