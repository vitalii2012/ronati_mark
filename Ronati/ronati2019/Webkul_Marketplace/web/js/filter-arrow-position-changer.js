define([
    'jquery',
    'mage/mage'
], function ($) {

    var productLists = $('.js-product-list');

    $('.js-check-dropdown, .js-check-dropdown .inventory-filter__check-icon').on('click', function (e) {
        e.preventDefault();

        var checkList = $(this).children('.js-check-list');

        if ($(e.target).hasClass('js-check-checkbox')) {
            if ($(e.target).prev('input:checked').length) {
                uncheckedAll(e);
            } else {
                productLists.find('input[type="checkbox"]').prop("checked", true);
                checkedAll(e);
            }
            $('.js-check-list').hide();

            return;
        }
        if (checkList.is(':visible')) {
            checkList.hide();
            $(this).removeClass('open');
        } else {
            $('.js-check-list').hide();
            checkList.toggle();
            $(this).toggleClass('open');
        }

    });

    $('#dropdown_all_check li').on('click', function (e) {

        var parrentBlock = $(this).parents('.js-check-dropdown');
        $('#dropdown_all_check li').removeClass('active');
        if ($(e.target).hasClass('all-products-check')) {
            parrentBlock.find('input[type="checkbox"]').prop("checked", true);
            checkedAll(e);
        } else if ($(e.target).hasClass('all-products-uncheck')) {
            parrentBlock.find('input[type="checkbox"]').prop("checked", false);
            uncheckedAll(e);

        }
        $(e.target).addClass('active');
        parrentBlock.removeClass('open');
    });

    function uncheckedAll(e) {
        productLists.find('input[type="checkbox"]').prop("checked", false);
        $('.js-filter-inventory').find('.js-inventory-btn').attr('disabled', '');
        $(e.target).prev('input').prop("checked", false);
    }

    function checkedAll(e) {
        productLists.find('input[type="checkbox"]').prop("checked", true);
        $('.js-filter-inventory').find('.js-inventory-btn').removeAttr('disabled');
        $(e.target).prev('input').prop("checked", true);
    }
});
