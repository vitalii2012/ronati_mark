define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('ronati.saveDraftButton', {

        _create: function () {
            var self = this.element
            $(self).on('change paste keyup', function () {
                var value = $(this).val()
                if (value.length < 1) {
                    $('.as_draft').addClass('unActive');
                } else {
                    $('.as_draft').removeClass('unActive');
                }

            })

        }

    });

    return $.ronati.saveDraftButton;
});



