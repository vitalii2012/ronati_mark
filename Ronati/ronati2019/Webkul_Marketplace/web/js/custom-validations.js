define([
    'jquery',
    'Magento_Ui/js/modal/alert'
], function ($, alert) {
    'use strict';

    return {
        callVerifySkuAjaxFunction: function (elem, validationUrl, ajaxErrorMessage , skuErrorMessage) {
            if (!elem.val().length) {
                alert({
                    content: skuErrorMessage || ''
                });
                $('div#skuavail').css('display','none');
                $('div#skunotavail').css('display','none');

                return
            }

            $.ajax({
                url: validationUrl,
                type: "POST",
                data: {
                    sku:elem.val()
                },
                dataType: 'html',
                success:function ($data) {
                    $data=JSON.parse($data);
                    if ($data.avialability == 1) {
                        $('div#skuavail').css('display','block');
                        $('div#skunotavail').css('display','none');
                    } else {
                        $('div#skunotavail').css('display','block');
                        $('div#skuavail').css('display','none');
                        elem.attr('value','');
                    }
                },
                error: function (response) {
                    alert({
                        content: ajaxErrorMessage || ''
                    });
                }
            });
        },
    }
});
