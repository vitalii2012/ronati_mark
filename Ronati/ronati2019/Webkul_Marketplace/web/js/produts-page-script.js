require([
    'jquery',
    'mage/url',
    'mage/mage',
    'domReady!'
], function ($, url) {

        return function () {

        $('#export_button').on("click", function () {
            $('#export_options').toggle();
        });

        $('#export_btn_options_csv').on("click", function () {
            $('#export_options').hide();
        });
        $('#export_btn_options_exel').on("click", function () {
            $('#export_options').hide();
        });
        $('#export_btn_options_pdf').on("click", function () {
            $('#export_options').hide();
        });
        $('body').on("click", function (e) {
            $('#export_options').hide();
        });

        $("#export_button_div").click(function (e) {
            e.stopPropagation();
        });

        if ($('#empty-inventory-div').hasClass("active")) {

            $('#check-all').attr("disabled", "disabled");
        }

        $('#add_items_with_marketplace').click(function () {
            $('.success message').hide();
            var add_to_marketplace_popup_counter = 0;
            $(".marketplace-add").each(function () {
                if ($(this).is(':checked')) {
                    add_to_marketplace_popup_counter++;
                }
            });
            if (add_to_marketplace_popup_counter > 0) {
                $('#no-marketplace-error').hide();
                // Add to marketplace product
                var values = [];
                $("input[name='product-list']:checked").each(function () {
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='add_item_marketplace']:checked").each(function () {
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");
                $('#add_items_with_marketplace').text('Wait...');

                $.ajax({
                    url: url.build('marketplace/product/addtomarketplace'),
                    type: 'POST',
                    data: {'productIds': str, 'marketplaceIds': marketplace_id},
                    success: function (returndata) {
                        $('#add_items_with_marketplace').text('Added...');
                        $('.success message').show();
                        location.reload();
                    }
                });
            } else {
                $('#no-marketplace-error').show();
            }
        });

        $('#remove_item_from_marketplace').click(function () {
            $('.success message').hide();
            var remove_from_marketplace_popup_counter = 0;
            $(".marketplace-remove").each(function () {
                if ($(this).is(':checked')) {
                    remove_from_marketplace_popup_counter++;
                }
            });
            if (remove_from_marketplace_popup_counter > 0) {
                $('#no-channel-error').hide();
                // remove from marketplace product
                var values = [];
                $("input[name='product-list']:checked").each(function () {
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='remove_item_marketplace_title']:checked").each(function () {
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");
                $('#remove_item_from_marketplace').text('Wait...');

                $.ajax({
                    url: url.build('marketplace/product/removefrommarketplace'),
                    type: 'POST',
                    data: {'productIds': str, 'marketplaceIds': marketplace_id},
                    success: function (returndata) {
                        $('#remove_item_from_marketplace').text('Removed...');
                        $('.success message').show();
                        location.reload();
                    }
                });

            } else {
                $('#no-channel-error').show();
            }
        });


        $('#close_button').on('click', function () {
            $('#disconnect').hide();
        });

        $('#no-thanks').on('click', function () {
            $('#disconnect').hide();
        });

        $('#close_on_hold_button').on('click', function () {
            //$('#hold_form').reset();
            //document.getElementById("date_on_hold").reset();
            $("input[name='product-list']:checked").prop('checked', false);
            $('#hidden_hold_popup').hide();
        });

        $('#close_button_remove_item').on('click', function () {
            $('#remove_item').hide();
        });


        $('#close_button_add_item').on('click', function () {
            $('#add_item').hide();
        });


        $('#close_button_sold').on('click', function () {
            $('#mark_as_sold_popup').hide();
        });

//enable/disbale action topbar

        $('.product-list-check').click(function () {
            var counter = 0;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    counter++;
                }
            });
            if (counter > 0) {
                $('.js-inventory-btn').attr('disabled', false);
            } else {
                $('.js-inventory-btn').attr('disabled', true);
            }
        });


// check all checkbox of all product in product list
        $('#check-list').click(function (event) {
            if (this.checked) {
                // Iterate each checkbox
                $(':checkbox').each(function () {
                    this.checked = true;
                    $('.js-inventory-btn').attr('disabled', false);
                });
            } else {
                $(':checkbox').each(function () {
                    this.checked = false;
                    $('.js-inventory-btn').attr('disabled', true);
                });
            }
        });




// deactivate product
        $('.deactivate_product').on("click", function () {
            //$('.deactivate_product').attr('disabled', true);
            $('.success message').hide();
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");
            $('#deactivate_link').text('Wait...');

            $.ajax({
                url: url.build('marketplace/product/deactivate'),
                type: 'POST',
                showLoader: true,
                data: {'productIds': str},
                success: function (returndata) {
                    $('#deactivate_link').text('Deactivated...');
                    $('.success message').show();
                    location.reload();
                }
            });
        });

// check all checkbox of all product in product list
        $('#check-list').click(function (event) {
            if (this.checked) {
                // Iterate each checkbox
                $(':checkbox').each(function () {
                    this.checked = true;
                    $('.js-inventory-btn').attr('disabled', false);
                });
            } else {
                $(':checkbox').each(function () {
                    this.checked = false;
                    $('.js-inventory-btn').attr('disabled', true);
                });
            }
        });
//activate product
        $('.activate_product').on("click", function () {
            $('.success message').hide();
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");
            $('#activate_link').text('Wait...');

            $.ajax({
                url: url.build('marketplace/product/activate'),
                type: 'POST',
                showLoader: true,
                data: {'productIds': str},
                success: function (returndata) {
                    $('#activate_link').text('Activated...');
                    $('.success message').show();
                    location.reload();
                }
            });
        });

        $('#on_hold_product_inventory').on("click", function () {

            $('.success message').hide();
            $('#on_hold_product_inventory').text('Wait...');

            var date = $('#date_on_hold').val();
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });
            var str = values.join(",");
            $.ajax({
                url: url.build('marketplace/product/onhold'),
                type: 'POST',
                data: {'productIds': str, 'date': date},
                success: function (returndata) {
                    $('.success message').show();
                    location.reload();
                }
            });
        });


        $('#delete_product_inventory').on("click", function () {
            $('.success message').hide();
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");


            $.ajax({
                url: url.build('marketplace/product/deleteproduct'),
                type: 'POST',
                data: {'productIds': str},
                success: function (returndata) {
                    $('.success message').show();
                    location.reload();
                }
            });
        });


        // duplicate product product
        $('.duplicate_product').on("click", function () {
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");

            $.ajax({
                url: url.build('marketplace/product/duplicate'),
                type: 'POST',
                data: {'productIds': str},
                success: function (returndata) {
                    location.reload();
                }
            });
        });

        $(document).click(function () {
            $('.js-check-list').hide();
        });

        $(".js-check-list").click(function (e) {
            e.stopPropagation();
        });

        $(".default-checkbox").click(function (e) {
            e.stopPropagation();
        });

        $('#default-checkbox').on("click", function (e) {
            $('#check-dropdown').toggle().css('visibility', 'visible');
            //$('#check-dropdown').css('visibility', 'visible')
        });

        //Redirect to import screen

        $('#import-button').click(function () {
            window.location.href = url.build('marketplace/product/import')
        });

        var sThisVal = [];
        $('input.default-checkbox__label:checkbox:checked').each(function () {
            sThisVal = $(this).val();
        })
    }

});
