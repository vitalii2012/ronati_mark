define([
    'jquery',
    'mage/mage'
], function ($) {

    return function (conf) {

        $('#delete_product_inventory').on("click", function () {

            $(this).addClass('disable');
            $('.success message').hide();
            $(this).text('Wait...');
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });
            var str = values.join(",");

            $.ajax({
                url: conf.url,
                type: 'POST',
                data: {'productIds': str},
                success: function (returndata) {
                    $('.success message').show();
                    $('#delete_product_inventory').text('Deleted...');
                    if(conf.page === 'reload'){
                        location.reload();
                    }else {
                        window.location.href = conf.page;
                    }
                }
            });
        });
    }
});
