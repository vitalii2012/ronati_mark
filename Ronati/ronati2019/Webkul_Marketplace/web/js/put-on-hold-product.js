define([
    'jquery',
    'mage/mage'
], function ($) {
    return function (conf) {
        $('#on_hold_product_inventory').on("click", function(){
            $('#on_hold_product_inventory').text('Wait...');
            var date = $('#date_on_hold').val();
            $.ajax({
                url: conf.url,
                type: 'POST',
                data: {'productIds': conf.product_id, 'date':date},
                success: function (returndata) {
                    window.location.href = conf.page;
                }
            });
        });
    }
});
