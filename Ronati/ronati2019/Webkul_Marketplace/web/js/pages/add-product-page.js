define([
        'jquery',
        'prototype',
        'domReady!'],
    function ($) {
            return function (conf) {
        trimoptiontext();

        $('#save-setting-close_button').click(function () {
            $('#save-setting-popup').hide();
        });

        var preChangeFormContent = "";
        var postChangeFormContent = "";
        var clicked_element_url = "";

        $(document).ready(function () {
            preChangeFormContent = $('#marketplace_setting_form_tag').serialize();
        });

        $('a').click(function (e) {
            var linkClass = $(this).attr('id');
            clicked_element_url = $(this).attr('href');
            console.log(clicked_element_url);
            if (!$("#" + linkClass).hasClass('cancel_nav')) {
                postChangeFormContent = $('#marketplace_setting_form_tag').serialize();

                if (preChangeFormContent != postChangeFormContent) {

                    e.preventDefault();
                    document.getElementById('save-setting-popup').style.display = "block";
                }
            }
        });

        $('#discard-changes-button').click(function () {
            window.location.href = clicked_element_url;
        });
        $('#cancel-leave-button').click(function () {
            document.getElementById('save-setting-popup').style.display = "none";
        });
        $('#save-confirmation-button').click(function () {
            document.getElementById('save-setting-popup').style.display = "none";
            $('.next-button').each(function () {
                if ($(this).is(":visible")) {
                    var Next_id = $(this).attr('id');
                    var id = Next_id.split("_");
                    nextButton(id[1]);
                }
            });
        });

        $('#marketplace-nav_0').addClass('active');
        $('#marketplaces_product_div_0').show();

        // price validation
        $(".price-box").on("keyup", function () {
            var valid = /^\d{0,8}(\.\d{0,2})?$/.test(this.value);
            if (!valid) {
                alert("Invalid price");
                this.value = '';
            }
        });

        let colorBlock = $('span:contains("Colors*")');
        if(colorBlock.length !== 0){
            colorBlock.parents('.select-multiple').addClass('color-wrapper');
            let select = $('.color-wrapper').find('select.multi');

            select.html(select.find('option').sort(function(x, y) {
                return $(x).text() > $(y).text() ? 1 : -1;
            }));
        }
    }
});
