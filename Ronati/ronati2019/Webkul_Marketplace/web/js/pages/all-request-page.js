define([
    'jquery',
    'mage/mage'
], function ($) {

    return function (conf) {

        $('#delete_request').on("click", function () {
            var requestId = $(this).attr('data-id');
            alert(requestId);
            $.ajax({
                url: conf.deleteRequests,
                type: 'POST',
                data: {request_id: requestId},
                cache: false,
                success: function (returndata) {
                    $('.screen-message').html('<span class="mage-success-message">Your Request has been deleted successfully!</span>');
                }
            });
            return false;
        });

        $('#reply_request').on("click", function () {
            var requestId = $(this).attr('data-id');
            alert(requestId);
            $.ajax({
                url: conf.replayRequests,
                type: 'POST',
                data: {request_id: requestId},
                cache: false,
                success: function (returndata) {
                    $('.screen-message').html('<span class="mage-success-message">You message has been send to buyer successfully!</span>');
                }
            });
            return false;
        });

        $('#save_request').on("click", function () {
            var requestId = $(this).attr('data-id');
            alert(requestId);
            $.ajax({
                url: conf.saveRequests,
                type: 'POST',
                data: {request_id: requestId},
                cache: false,
                success: function (returndata) {
                    $('.screen-message').html('<span class="mage-success-message">Your request has been saved successfully!</span>');
                }
            });
            return false;
        });

        $('.check-dropdown').on('click', '.check-dropdown__item', function (e) {
            e.preventDefault();
            $('.check-dropdown__item').removeClass('active');
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
            }
        });

        if ($('.js-check-dropdown').length) {
            $(document).on('click', function (e) {
                if (!$(e.target).closest(".js-check-dropdown").length && !$(e.target).closest(".js-check-list").length) {
                    $('.js-check-list').hide();
                    $('.js-check-dropdown').removeClass('open');
                }
            });
        }
    }
});
