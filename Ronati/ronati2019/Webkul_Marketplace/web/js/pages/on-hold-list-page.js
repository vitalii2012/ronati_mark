require([
    'jquery',
    'mage/mage'
], function ($) {

    return function (conf) {
        $('#export_button').on("click", function () {
            $('#export_options').toggle();
        });

        $('#export_btn_options_csv').on("click", function () {
            $('#export_options').hide();
        });
        $('#export_btn_options_exel').on("click", function () {
            $('#export_options').hide();
        });
        $('#export_btn_options_pdf').on("click", function () {
            $('#export_options').hide();
        });
        $('body').on("click", function (e) {
            $('#export_options').hide();
        });

        $("#export_button_div").click(function (e) {
            e.stopPropagation();
        });

        if ($('#empty-inventory-div').hasClass("active")) {

            $('#check-all').attr("disabled", "disabled");
        }
        $("#add-to-marketplace").click(function () {
            var product_ids = [];
            var str;
            var split = [];
            var counter = 0;
            var i = 1, j = 0;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    if ($('#product-status_' + i).val() != "active") {
                        counter++;
                    } else {
                        split = $(this).attr('id').split("_");
                        product_ids[j] = split[1];
                        j++;
                    }
                }
                i = i + 2;
            });
            str = product_ids.toString();
            if (counter > 0) {
                $("#market-add").fadeIn('slow').delay(5000).fadeOut('slow');
                $(window).scrollTop($('#header').offset().top);
            } else {

                showAddItemPopup(str);
            }

        });

        $("#remove-from-marketplace").click(function () {
            var counter = 0;
            var i = 1;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    if ($('#product-status_' + i).val() != "active") {
                        counter++;
                    }
                }
                i = i + 2;
            });


            if (counter > 0) {
                $("#market-remove").fadeIn('slow').delay(5000).fadeOut('slow');
                $(window).scrollTop($('#header').offset().top);
            } else {
                showRemoveItemPopup();
            }

        });

        var url = $(location).attr('href');
        let parts = url.split("/");
        let last_part = parts[parts.length - 1];
        if (last_part == 'imported') {
            location.href = conf.productList;
        }

        //add to marketplace
        $('#add_items_with_marketplace').click(function () {
            var add_to_marketplace_popup_counter = 0;

            $('.success message').hide();
            $(".marketplace-add").each(function () {
                if ($(this).is(':checked')) {
                    add_to_marketplace_popup_counter++;
                }
            });
            if (add_to_marketplace_popup_counter > 0) {
                $('#no-marketplace-error').hide();
                // Add to marketplace product
                var values = [];
                $("input[name='product-list']:checked").each(function () {
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='add_item_marketplace']:checked").each(function () {
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");

                var split;
                $.each(values, function (index, value) {
                    $(".marketplace-add").each(function () {
                        if ($(this).is(':checked')) {
                            split = $(this).attr('id').split("_");
                            $('#new-shipping-method_' + split[1]).show();
                            $('#default-check-hide-details_' + split[1]).show();
                        }
                    });
                });
                if ($('#product-marketplaces-settings').valid()) {
                    var datastring = $('#product-marketplaces-settings').serialize(),
                    data = datastring + "&productIds=" + str + "&marketplaceIds=" + marketplace_id;
                    $('#add_items_with_marketplace').text('Wait...');
                    $('#add_items_with_marketplace').attr("disabled", true);
                    $.ajax({
                        url: conf.addToMarketplace,
                        type: 'POST',
                        data: data,
                        success: function (returndata) {
                            $('.success message').show();
                            location.reload();
                        }
                    });
                }
            } else {
                $('#no-marketplace-error').show();
            }
        });

        //remove from marketplace
        $('#remove_item_from_marketplace').click(function () {
            $('.success message').hide();
            var remove_from_marketplace_popup_counter = 0;
            $(".marketplace-remove").each(function () {
                if ($(this).is(':checked')) {
                    remove_from_marketplace_popup_counter++;
                }
            });
            if (remove_from_marketplace_popup_counter > 0) {
                $('#no-channel-error').hide();
                // remove from marketplace product
                var values = [];
                $('#remove_item_from_marketplace').text('Wait...');
                $('#remove_item_from_marketplace').attr("disabled", true);
                $("input[name='product-list']:checked").each(function () {
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='remove_item_marketplace_title']:checked").each(function () {
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");

                $.ajax({
                    url: conf.removeFromMarketplace,
                    type: 'POST',
                    data: {'productIds': str, 'marketplaceIds': marketplace_id},
                    success: function (returndata) {
                        $('.success message').show();
                        location.reload();
                    }
                });

            } else {
                $('#no-channel-error').show();
            }
        });


        $('.product-list-check').click(function () {
            var counter = 0;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    counter++;
                }
            });
            if (counter > 0) {
                $('#editstatus').attr('disabled', false);
                $('.delete_product').attr('disabled', false);
            } else {
                $('.js-inventory-btn').attr('disabled', true);
            }
        });

        $('#check-list').click(function (event) {
            if (this.checked) {
                // Iterate each checkbox
                $('input[name=product-list]').each(function () {
                    this.checked = true;
                    $('#editstatus').attr('disabled', false);
                    $('.delete_product').attr('disabled', false);
                });
            } else {
                $(':checkbox').each(function () {
                    this.checked = false;
                    $('.js-inventory-btn').attr('disabled', true);
                });
            }
        });

        $('#checkbox_icon').on("click", function () {
            //$('#dropdown_all_check').toggle();
            $('#dropdown_all_check').toggle().css('visibility', 'visible');
        });

        $('#close_button').on('click', function () {
            $('#disconnect').hide();
        });

        $('#no-thanks').on('click', function () {
            $('#disconnect').hide();
        });

        $('#close_button_remove_item').on('click', function () {
            $('#remove_item').hide();
        });

        $('#close_on_hold_button').on('click', function () {
            $("input[name='product-list']:checked").prop('checked', false);
            $('#hidden_hold_popup').hide();
        });


        $('#close_button_add_item').on('click', function () {
            $('#product-marketplaces').html('');
            $('#add_item').hide();
        });

        //close mark as sold popup
        $('#close_button_sold').on('click', function () {
            $('#marketplace_channel_list').html('');
            $("input[name='product-list']:checked").prop('checked', false);
            $('#mark_as_sold_popup').hide();
            return false;
        });

        $('#close_button_remove_item').on('click', function () {
            $('#remove_item').hide();
        });

        // duplicate product product
        $('.duplicate_product').on("click", function () {
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");

            $.ajax({
                url: conf.duplicateProduct,
                type: 'POST',
                data: {'productIds': str},
                success: function (returndata) {
                    location.reload();
                }
            });
        });
        $(document).click(function () {
            $('.js-check-list').hide();
        });

        $(".js-check-list").click(function (e) {
            e.stopPropagation();
        });

        //Redirect to import screen
        $('#import-button').click(function () {
            window.location.href = conf.importProducts;
        });
    }
});
