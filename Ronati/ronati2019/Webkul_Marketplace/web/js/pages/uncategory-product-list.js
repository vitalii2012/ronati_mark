define([
    'jquery',
    'mage/mage'
], function ($) {

    return function (conf) {
        $('#export_btn_options_csv_a').on("click", function () {
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });
            var str = values.join(",");
            $.ajax({
                url: conf.exportLink,
                type: 'POST',
                showLoader: true,
                data: {'productIds': str},
                success: function (returndata) {
                    // get current date
                    var today = new Date();
                    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    var dateTime = date + '_' + time;
                    // End Current date
                    var filename = 'seller_product_' + dateTime + '.csv';
                    var rows = JSON.parse(returndata);
                    var processRow = function (row) {
                        var finalVal = '';
                        for (var j = 0; j < row.length; j++) {
                            var innerValue = row[j] === null ? '' : row[j].toString();
                            if (row[j] instanceof Date) {
                                innerValue = row[j].toLocaleString();
                            }
                            var result = innerValue.replace(/"/g, '""');
                            if (result.search(/("|,|\n)/g) >= 0)
                                result = '"' + result + '"';
                            if (j > 0)
                                finalVal += ',';
                            finalVal += result;
                        }
                        return finalVal + '\n';
                    };

                    var csvFile = '';
                    for (var i = 0; i < rows.length; i++) {
                        csvFile += processRow(rows[i]);
                    }

                    var blob = new Blob([csvFile], {type: 'text/csv;charset=utf-8;'});
                    if (navigator.msSaveBlob) {
                        navigator.msSaveBlob(blob, filename);
                    } else {
                        var link = document.createElement("a");
                        if (link.download !== undefined) { // feature detection
                            var linkUrl = URL.createObjectURL(blob);
                            link.setAttribute("href", linkUrl);
                            link.setAttribute("download", filename);
                            link.style.visibility = 'hidden';
                            document.body.appendChild(link);
                            link.click();
                            document.body.removeChild(link);
                        }
                    }
                    location.reload();
                }
            });
        });

        $('#export_btn_options_xlsx_a').on("click", function () {
            //$("#export_loader").show();
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });
            var str = values.join(",");
            $.ajax({
                url: conf.exportLink,
                type: 'POST',
                showLoader: true,
                data: {'productIds': str},
                success: function (returndata) {
                    // get current date
                    var today = new Date();
                    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    var dateTime = date + '_' + time;
                    // End Current date
                    var filename = 'seller_product_' + dateTime + '.xls';
                    var rows = JSON.parse(returndata);
                    var processRow = function (row) {
                        var finalVal = '';
                        for (var j = 0; j < row.length; j++) {
                            var innerValue = row[j] === null ? '' : row[j].toString();
                            if (row[j] instanceof Date) {
                                innerValue = row[j].toLocaleString();
                            }
                            var result = innerValue.replace(/"/g, '""');
                            if (result.search(/("|,|\n)/g) >= 0)
                                result = '"' + result + '"';
                            if (j > 0)
                                finalVal += ',';
                            finalVal += result;
                        }
                        return finalVal + '\n';
                    };

                    var xlsxFile = '';
                    for (var i = 0; i < rows.length; i++) {
                        xlsxFile += processRow(rows[i]);
                    }

                    var blob = new Blob([xlsxFile], {type: 'text/xls;charset=utf-8;'});
                    if (navigator.msSaveBlob) { // IE 10+
                        navigator.msSaveBlob(blob, filename);
                    } else {
                        var link = document.createElement("a");
                        if (link.download !== undefined) { // feature detection
                            // Browsers that support HTML5 download attribute
                            var urlLink = URL.createObjectURL(blob);
                            link.setAttribute("href", urlLink);
                            link.setAttribute("download", filename);
                            link.style.visibility = 'hidden';
                            document.body.appendChild(link);
                            link.click();
                            document.body.removeChild(link);
                        }
                    }
                    location.reload();
                }
            });
        });

        $('#export_btn_options_pdf_a').on("click", function () {
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });
            var str = values.join(",");
            $.ajax({
                url: conf.exportPDFLink,
                type: 'POST',
                data: {'productIds': str},
                success: function (returndata) {
                }
            });
        });


        $('#export_button').on("click", function () {
            $('#export_options').toggle();
        });

        $('#export_btn_options_csv').on("click", function () {
            $('#export_options').hide();
        });
        $('#export_btn_options_exel').on("click", function () {
            $('#export_options').hide();
        });
        $('#export_btn_options_pdf').on("click", function () {
            $('#export_options').hide();
        });
        $('body').on("click", function (e) {
            $('#export_options').hide();
        });

        $("#export_button_div").click(function (e) {
            e.stopPropagation();
        });

        $("#product-marketplaces-settings").submit(function (e) {

            var add_to_marketplace_popup_counter = 0;
            $(".marketplace-add").each(function () {
                if ($(this).is(':checked')) {
                    add_to_marketplace_popup_counter++;
                }
            });
            if (add_to_marketplace_popup_counter > 0) {
                $('#no-marketplace-error').hide();
            } else {
                e.preventDefault();
                $('#no-marketplace-error').show();
            }

        });
        
        if ($('#empty-inventory-div').hasClass("active")) {

            $('#check-all').attr("disabled", "disabled");
        }

        $("#add-to-marketplace").click(function () {
            var product_ids = [];
            var str;
            var split = [];
            var counter = 0;
            var checked_products_marketplaces = [];
            var i = 1, j = 0;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    if ($('#product-status_' + i).val() != "active") {
                        counter++;
                    } else {
                        if (typeof $('#product-channel_' + i).val() !== 'undefined') {
                            var marketplace_id_array = $('#product-channel_' + i).val().split(",");
                            $.each(marketplace_id_array, function (index, value) {
                                if (typeof checked_products_marketplaces[value] === 'undefined') {
                                    checked_products_marketplaces[value] = 1;
                                } else {
                                    checked_products_marketplaces[value] = parseInt(checked_products_marketplaces[value]) + 1;
                                }
                            });
                        }
                        split = $(this).attr('id').split("_");
                        product_ids[j] = split[1];
                        j++;
                    }
                }
                i = i + 2;
            });
            str = product_ids.toString();
            if (counter > 0) {
                $("#market-add").fadeIn('slow').delay(5000).fadeOut('slow');
                $(window).scrollTop($('#header').offset().top);
            } else {

                showAddItemPopup(str, checked_products_marketplaces);
            }

        });

        //remove from marketplace
        $("#remove-from-marketplace").click(function () {
            var counter = 0;
            var i = 1;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    if ($('#product-status_' + i).val() != "active") {
                        counter++;
                    }
                }
                i = i + 2;
            });


            if (counter > 0) {
                $("#market-remove").fadeIn('slow').delay(5000).fadeOut('slow');
                $(window).scrollTop($('#header').offset().top);
            } else {
                showRemoveItemPopup();
            }

        });

        // //Load list after import
        var url = $(location).attr('href'),
        parts = url.split("/"),
        last_part = parts[parts.length - 1];
        if (last_part == 'imported') {
            location.href = conf.productListPage;
        }
        //add to marketplace
        $('#add_items_with_marketplace').click(function () {
            var add_to_marketplace_popup_counter = 0;

            $('.success message').hide();
            $(".marketplace-add").each(function () {
                if ($(this).is(':checked')) {
                    add_to_marketplace_popup_counter++;
                }
            });
            if (add_to_marketplace_popup_counter > 0) {
                $('#no-marketplace-error').hide();
                // Add to marketplace product
                var values = [];
                $("input[name='product-list']:checked").each(function () {
                    values.push($(this).val());
                });
                var str = values.join(",");
                $('#product_ids_value').val(str);
                var marketplace = [];
                $("input[name='add_item_marketplace']:checked").each(function () {
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");

                var split;
                $.each(values, function (index, value) {
                    $(".marketplace-add").each(function () {
                        if ($(this).is(':checked')) {
                            split = $(this).attr('id').split("_");
                            $('#new-shipping-method_' + split[1]).show();
                            $('#default-check-hide-details_' + split[1]).show();
                        }
                    });
                });
                if ($('#product-marketplaces-settings').valid()) {
                    var datastring = $('#product-marketplaces-settings').serialize(),
                    data = datastring + "&productIds=" + str + "&marketplaceIds=" + marketplace_id;
                    $('#add_items_with_marketplace').text('Wait...');
                    $('#add_items_with_marketplace').attr("disabled", true);
                    $.ajax({
                        url: "<?= $this->getUrl('marketplace/product/addproducttomarketplace'); ?>",
                        type: 'POST',
                        data: data,
                        success: function (returndata) {
                            window.location = "<?= $this->getUrl('marketplace/product/addproducttomarketplace'); ?>";
                        }
                    });
                }
            } else {
                $('#no-marketplace-error').show();
            }
        });

        //remove from marketplace
        $('#remove_item_from_marketplace').click(function () {
            $('.success message').hide();
            var remove_from_marketplace_popup_counter = 0;
            $(".marketplace-remove").each(function () {
                if ($(this).is(':checked')) {
                    remove_from_marketplace_popup_counter++;
                }
            });
            if (remove_from_marketplace_popup_counter > 0) {
                $('#no-channel-error').hide();
                // remove from marketplace product
                var values = [];
                $('#remove_item_from_marketplace').text('Wait...');
                $('#remove_item_from_marketplace').attr("disabled", true);
                $("input[name='product-list']:checked").each(function () {
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='remove_item_marketplace_title']:checked").each(function () {
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");

                $.ajax({
                    url: conf.removeFromMP,
                    type: 'POST',
                    data: {'productIds': str, 'marketplaceIds': marketplace_id},
                    success: function (returndata) {
                        $('.success message').show();
                        window.location = conf.inventoryPage;
                    }
                });

            } else {
                $('#no-channel-error').show();
            }
        });


        //enable/disbale action topbar

        $('.product-list-check').click(function () {
            var counter = 0;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    counter++;
                }
            });
            if (counter > 0) {
                $('.js-inventory-btn').attr('disabled', false);
            } else {
                $('.js-inventory-btn').attr('disabled', true);
            }
        });


        // check all checkbox of all product in product list
        $('#check-list').click(function (event) {
            if (this.checked) {
                // Iterate each checkbox
                $('input[name=product-list]').each(function () {
                    this.checked = true;
                    $('.js-inventory-btn').attr('disabled', false);
                });
            } else {
                $(':checkbox').each(function () {
                    this.checked = false;
                    $('.js-inventory-btn').attr('disabled', true);
                });
            }
        });
        // end check all checkbox of all product in product list

        $('#checkbox_icon').on("click", function () {
            //$('#dropdown_all_check').toggle();
            $('#dropdown_all_check').toggle().css('visibility', 'visible');
        });

        $('#close_button').on('click', function () {
            $('#disconnect').hide();
        });

        $('#no-thanks').on('click', function () {
            $('#disconnect').hide();
        });

        $('#close_button_remove_item').on('click', function () {
            $('#remove_item').hide();
        });

        $('#close_on_hold_button').on('click', function () {

            $('#hidden_hold_popup').hide();
        });


        $('#close_button_add_item').on('click', function () {
            $('#product-marketplaces').html('');
            $('#add_item').hide();
        });
        $('#close_button_add_item').on('click', function (e) {
            e.preventDefault();
            $('#add_item').hide();
            return false;
        });
        //close mark as sold popup
        $('#close_button_sold').on('click', function () {
            $('#marketplace_channel_list').html('');
            $("input[name='product-list']:checked").prop('checked', false);
            $('#mark_as_sold_popup').hide();
            return false;
        });

        $('#close_button_remove_item').on('click', function () {
            $('#remove_item').hide();
        });

        // deactivate product
        $('.deactivate_product').on("click", function () {
            //$('.deactivate_product').attr('disabled', true);
            $('.success message').hide();
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");

            $.ajax({
                url: conf.deactivateProduct,
                type: 'POST',
                showLoader: true,
                data: {'productIds': str},
                success: function (returndata) {
                    $('.success message').show();
                    location.reload();
                }
            });
        });

        //activate product
        $('.activate_product').on("click", function () {
            $('.success message').hide();
            // By Sushil Date: 14/Sep/2018
            var values = [];
            var status = [];
            var needle = 4;
            var needle_uncategorized = 7;
            $("input[name='product-list']:checked").each(function () {
                status.push($(this).attr('data-id'));
                values.push($(this).val());
            });
            for (var i = 0; i < status.length; i++) {
                if (status[i] == needle || status[i] == needle_uncategorized) {
                    $("#product-draft").fadeIn('slow').delay(5000).fadeOut('slow');
                    $(window).scrollTop($('#header').offset().top);
                    return false;
                }
            }

            var str = values.join(",");

            $.ajax({
                url: conf.activateProduct,
                type: 'POST',
                showLoader: true,
                data: {'productIds': str},
                success: function (returndata) {
                    $('.success message').show();
                    location.reload();
                }
            });
        });

        // duplicate product product
        $('.duplicate_product').on("click", function () {
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");

            $.ajax({
                url: conf.duplicateProduct,
                type: 'POST',
                data: {'productIds': str},
                success: function (returndata) {
                    location.reload();
                }
            });
        });
        $(document).click(function () {
            $('.js-check-list').hide();
        });

        $(".js-check-list").click(function (e) {
            e.stopPropagation();
        });

        //Redirect to import screen
        $('#import-button').click(function () {
            window.location.href = conf.importLink;
        });
    }
});
