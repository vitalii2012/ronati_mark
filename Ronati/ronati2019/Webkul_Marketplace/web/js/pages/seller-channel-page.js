define([
    'jquery',
    'mage/mage',
    "filterableSelect"
], function ($) {

    return function (conf) {
        //Start Functions hide max reach popup
        $('#close-max-reach-popup').click(function () {
            $('#max-reach-popup').hide();
        });

        $('.multi-select-dd').fSelect();

        $('body').click(function (event) {
            if ($(event.target).hasClass('r-popup')) {
                var divsToHide = document.getElementsByClassName("r-popup");
                for (var i = 0; i < divsToHide.length; i++) {
                    divsToHide[i].style.display = "none";
                }
            }
        });

        $('#inventory-accordion__targer').click(function () {
            $('#inventory-accordion__wrap').toggle();
        });

        $('#inventory-accordion-marketplace').click(function () {
            $('#inventory-accordion-marketplace__wrap').toggle();
        });


        $('#submit-request').click(function () {
            if ($('#request-channel-form').valid()) {
                var confirmMessage = $('#request-confirmation-message');
                if (!$('#request-confirmation').is(':checked')) {
                    confirmMessage.show();
                    confirmMessage.html('Please accept Terms of Use and Ronati Privacy, Cookie and GDPR Policy');

                } else {
                    saveMarketplaceRequests();
                    confirmMessage.hide();
                    confirmMessage.html('');
                }
            }
        });
        var modal = document.getElementById('myModal');

        $('span').click = function () {
            modal.style.display = "none";
        };

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        };

        window.saveMarketplaceRequests = function () {
            var datastring = $('#request-channel-form').serialize();
            var submitRequest = $('#submit-request');
            submitRequest.val('Wait...');
            submitRequest.attr("disabled", true);
            $.ajax({
                url: conf.saveCustomRequest,
                showLoader: false,
                data: datastring,
                type: 'POST'
            }).success(function (data) {
                    $('#myModal').hide();
                    var overlay = document.getElementById('modal-overlay');
                    overlay.style.display = "none";
                    $('#website').val("http://");
                    $('#channel-name').val("");
                    $('#login').val("");
                    $('#seller_password').val("");
                    $('#confirm_password').val("");
                    $('#match-private-shop').attr('checked', false);
                    $('#request-confirmation').attr('checked', false); // Unchecks it
                    submitRequest.val('apply request');
                    submitRequest.attr("disabled", false);
                }
            ).error(function (data) {
                $('#myModal').hide();
                var overlay = document.getElementById('modal-overlay');
                overlay.style.display = "none";

            });
        };

        window.sendConnectMarketplaceData = function (id) {
            if ($('#connect-channel-form_' + id).valid()) {
                if ($('#confirm_' + id).is(':checked')) {
                    $('#confirm-error_' + id).hide();
                    var marketplaceId = id;
                    var formData = $('#connect-channel-form_' + marketplaceId).serializeArray();
                    formData.push({name: 'marketplaceId', value: marketplaceId});
                    $('.connect-channel-form__btn').val('Wait...');
                    $('.connect-channel-form__btn').attr("disabled", true);
                    $.ajax({
                        url: conf.connectMarketplace,
                        type: 'POST',
                        data: formData,
                        showLoader: false,
                        success: function (response) {
                            window.location = response.redirect;
                        },
                        error: function (request, textStatus, errorThrown) {
                            location.reload();
                        }
                    });
                    return false;
                } else {
                    $('#confirm-error_' + id).show();
                }
            } else {
                $('#confirm-error_' + id).hide();
            }
        };

        $('.connect-button').click(function () {
            if( $(this).hasClass('notAllow')){
                $("#max-channel-message").fadeIn('slow').delay(5000).fadeOut('slow');
                $(window).scrollTop($('#header').offset().top);
            }else {
                var id = $(this).attr('id');
                $('#error_' + id).css('display', 'none');
                $('#connect-channel-form_' + id)[0].reset();
                $('#connect_' + id).css('display', 'block');
            }
        });


        window.closeConnectPopup = function (id) {
            $('#default-settings_' + id).html('');
            $('#connect_' + id).css('display', 'none');
        };
    }
});
