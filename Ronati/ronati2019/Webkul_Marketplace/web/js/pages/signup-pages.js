define([
        'jquery',
        'mage/mage',
        'tagManager'
    ], function($) {
        return function (conf) {

            $('#seller_password-error').html("");
            var n = Math.floor(10 * Math.random()),
                r = Math.floor(10 * Math.random()),
                c = n + r;
            $('#digit').text(n + " + " + r + " =");

            //custom validation for website url by MS(20june2018)
            var PassWebsite=0;
            $('#seller-third-next-button').click(function()
            {

                if($('#captcha').val() != c) {
                    $('#captcha-message').show();
                    $('#captcha-message').text('Invalid Answer');
                    //$('#captcha').val('');
                    return false;
                } else {
                    $('#captcha-message').hide();
                }

                var checkurl=0;
                checkurl=isValidUrl($('#profileurl').val());
                if(checkurl==1||$('#profileurl').val()=="")
                {
                    $('#valid-url-error').hide();
                }
                else
                {
                    $('#valid-url-error').show();
                    $(window).scrollTop($('#company_name').offset().top);
                }

                if(checkurl==1 || $('#profileurl').val()=="")
                {
                    PassWebsite=1;
                }
                else
                {
                    PassWebsite=0;
                }
            });

            //Profile URL on focus
            $("#profileurl").focusin(function(){
                $("#profileurl").val('http://');
            });


            //Hide buyer signup error message
            $('#buyer-signup-message').hide();
            // Show hide buyer-seller form
            $('#seller-link').click(function() {
                $('#seller').show();
                $('#buyer').hide();

                $('#seller-link').addClass("active");
                $('#buyer-link').removeClass("active");

                $('.sign-up__title').text("Seller Application");
                $('.sign-up__sub-title').text("Apply to access a world of buyers searching for vintage & antique pieces worldwide.");
            });

            $('#buyer-link').click(function() {
                $('#buyer').show();
                $('#seller').hide();

                $('#seller-link').removeClass("active");
                $('#buyer-link').addClass("active");

                $('.sign-up__title').text("Sign up as buyer");
                $('.sign-up__sub-title').text("Join the community and start finding in seconds");
            });

            //Buyer Signup after validation
            $('#buyer-next-button').click(function() {
                if ($('#sign-up-buyer').valid()) {
                    if($('#confirmation-buyer').is(':checked')) {
                        signupNowProcess();
                    } else {
                        $('#confirmation-message').html('Please accept Terms of Use and Ronati Privacy, Cookie and GDPR Policy.');
                        return false;
                    }
                }
            });

            $("#confirmation-buyer").change(function() {
                if(this.checked) {
                    $('#confirmation-message').html('');
                }
            });

            //Seller Signup first step validation
            $('#seller-first-next-button').click(function() {

                if ($('#sign-up-seller').valid()) {
                    if(!$('#confirmation-seller').is(':checked')) {
                        $('#seller-confirmation-message').html('Please accept Terms of Use and Ronati Privacy, Cookie and GDPR Policy.');
                        return false;
                    } else {
                        //added due to Beta Phase
                        $('#sign-up-header').hide();
                        $('#sign-up-sub-title').hide();
                        $(window).scrollTop($('#pack_0').offset().top);
                        $('#seller-step-2').show();
                        $('#seller-step-1').hide();
                        $('#seller-steps__nav-link_1').removeClass('seller-steps__nav-link disabled');
                        $('#seller-steps__nav-link_1').addClass('seller-steps__nav-link');
                        $('#seller-confirmation-message').html('');
                    }
                }
            });

            //Seller Signup third step validation
            $('#seller-third-next-button').click(function() {

                var check_category = $('input[name="category_id[]"]:checked').length == 0;

                if($('#sign-up-seller').valid()&&(!check_category)){
                    $('#category-message').html('');
                    $('#shipping-message').html('');

                    //added due to BETA phase
                    $('#seller-steps__nav-link_2').removeClass('seller-steps__nav-link disabled');
                    //-----------------------------------------------------------------------------------
                    //commented due to Beta Phase
                    // $('#seller-steps__nav-link_3').removeClass('seller-steps__nav-link disabled');
                    $('#seller-steps__nav-link_1').addClass('seller-steps__nav-link');
                    $('#seller-steps__nav-link_2').addClass('seller-steps__nav-link');
                    //commented due to Beta Phase
                    //$('#seller-steps__nav-link_3').addClass('seller-steps__nav-link');

                    $('#category-message').hide();
                    $('#category-message').html('');
                    if(PassWebsite==1){sellersignupProcess();}
                }else{
                    if(check_category){
                        $('#category-message').html('<span class="mage-error">Please select category to continue</span>');}else{$('#category-message').html('');}
                }
            });

            //Seller signup back form actions

            $('#seller-steps__nav-link_1').click(function() {
                if($('#seller-steps__nav-link_2').hasClass('disabled')){
                    $('#seller-step-3').hide();
                    $('#seller-step-2').hide();
                    $('#seller-step-1').show();
                }
            });

            $('#seller-steps__nav-link_2').click(function() {

                if ($('#sign-up-seller').valid()) {
                    if($('#confirmation-seller').is(':checked')) {
                        //added due to Beta Phase
                        if($('#seller-steps').hasClass('disabled')){
                            $('#sign-up-header').hide();
                            $('#sign-up-sub-title').hide();
                            $(window).scrollTop($('#pack_0').offset().top);
                            $('#seller-step-2').show();
                            $('#seller-step-1').hide();
                            $('#seller-steps__nav-link_1').removeClass('seller-steps__nav-link disabled');
                            $('#seller-steps__nav-link_1').addClass('seller-steps__nav-link');
                            $('#seller-confirmation-message').html('');
                        }}else{$('#seller-confirmation-message').html('Please accept Terms of Use and Ronati Privacy, Cookie and GDPR Policy.');}

                }
            });
        }

    });


