define([
    'jquery',
    'mage/mage'
], function ($) {

    return function (conf) {
        window.onload = function () {
            trimoptiontext()
        };

        $('#first-setting-close_button').click(function () {
            $('#first-setting-popup').hide()
        });
        if ($('#default-settings').find('div').length === 0) {
            $.ajax({
                url: conf.defaultShipping,
                showLoader: false,
                data: {'marketplace_id': conf.channelLink},
                type: 'POST'
            }).success(function (data) {
                if (data === null || data.replace(/\s/g, '') === '') {
                    $('#no-setting-message').show();
                } else {
                    $('#no-setting-message').hide();
                }
                $('#default-settings').append(data);
                //FUNCTION TO CLOSE POPUP OF SAVE SETTING
                $('#save-setting-close_button').click(function () {
                    $('#save-setting-popup').hide()
                });
                $('.price-box').on('keyup', function () {
                    var valid = /^\d{0,8}(\.\d{0,2})?$/.test(this.value);
                    if (!valid) {
                        alert('Invalid price');
                        this.value = ''
                    }
                });
                var preChangeFormContent = '';
                var postChangeFormContent = '';

                $(document).ready(function () {
                    preChangeFormContent = $('#edit-setting-channel').serialize()

                });

                $('a').click(function (e) {
                    var linkId = $(this).attr('id');
                    if (linkId != 'discard-changes-button' && linkId != 'sales-disconnect-button') {
                        postChangeFormContent = $('#edit-setting-channel').serialize();

                        if (preChangeFormContent != postChangeFormContent) {

                            e.preventDefault();
                            document.getElementById('save-setting-popup').style.display = 'block'
                        }
                    }
                });

                $('#discard-changes-button').click(function () {
                    window.location.href = conf.productList;
                })

            }).error(function (data) {
                $('#no-setting-message').show();

            })
        }
        $('.multi-select-dd').fSelect();
        $('#close_button').on('click', function () {
            $('#disconnect').hide()
        });
        $('#no-thanks').on('click', function () {
            $('#disconnect').hide()
        });
        $('#Continue').on('click', function () {
            $('#first-setting-popup').hide()
        });
        $('#disconnect_channel').on('click', function () {
            $('#disconnect').css('display','none');
            $('#final-delete').css('display','block');
            $.ajax({
                url: conf.disconnectMarketplace,
                type: 'POST',
                data: {'channel_id': conf.channelLink},
                showLoader: false,
                success: function (returndata) {
                    $('#ok-thanks').on('click', function () {
                        $('#final-delete').hide();
                        window.location = conf.productList;
                    });
                    $('#final-close-button').on('click', function () {
                        $('#final-delete').hide()
                    })
                }
            })
        });

        $('#ok-thanks, #final-close-button').on('click', function () {
            $('#final-delete').hide();
            $('.loading-mask').addClass('is_showed');
            window.location = conf.productList;
        });

        function saveDetails() {
            if (!$('#edit-setting-channel').valid()) {

                $('.isCheckbox').each(function () {
                    if (!$(this).is(':checked')) {
                        //check weather conditional fields is turned off
                        if ($(this).hasClass('conditional-checkbox')) {
                            var part = $(this).attr('id').split('label')[1];
                            $('#conditional_div' + part + ' input').each(function () {
                                $(this).val('')
                            });
                            $('#conditional_div' + part + ' select').each(function () {
                                var dropdown_id = $(this).attr('id');
                                $('#' + dropdown_id + ' option').each(function () {
                                    $(this).prop('selected', false)
                                })
                            })
                        }
                        $(this).val('off');
                        $(this).attr('checked', true)
                    }
                });
                $('.conditional-dropdown').each(function () {
                    var value_selected = $(this).val();
                    var part = $(this).attr('id').split('dropdown')[1];
                    $(this).find('option').each(function () {
                        if (value_selected != $(this).val()) {
                            var option_text = $(this).val().replace(' ', '_');
                            $('#dropdown_option_conditional' + part + '_' + option_text + ' input').each(function () {
                                $(this).val('')
                            })
                        }
                    })
                });
            } else {
                var channel_id = conf.channelLink;
                var formData = $('#edit-setting-channel').serializeArray();
                formData.push({name: 'marketplaceId', value: channel_id});
                $.ajax({
                    url: conf.saveMarketplace,
                    type: 'POST',
                    data: formData,
                    showLoader: true,
                    success: function (returndata) {
                        var obj = JSON.parse(returndata);
                        if (obj.error) {
                            alert(obj.message)
                        } else {
                            window.location = conf.linkMarketplace;
                        }
                    }
                })
            }
        }

        function trimoptiontext() {
            jQuery('.form-item__dropdown').each(function () {
                if (!jQuery(this).hasClass('multi')) {
                    var option_data_id = jQuery(this).attr('id');
                    jQuery('#' + option_data_id + ' option').each(function () {
                        if (jQuery(this).text().length > 22) {
                            string = jQuery(this).text();
                            var new_option_text = string.substring(0, 20) + '...';
                            jQuery(this).text(new_option_text);
                            jQuery(this).attr('title', string)
                        }
                    })
                } else {
                    var option_id = jQuery(this).attr('id');
                    jQuery('#' + option_id + ' option').each(function () {
                        if (jQuery(this).text().length > 54) {
                            string = jQuery(this).text();
                            var new_option_text = string.substring(0, 52) + '...';
                            jQuery(this).text(new_option_text);
                            jQuery(this).attr('title', string)
                        }
                    })

                }
            })
        }

        $('#sales-disconnect-button').click(function () {
            $('#disconnect').css('display','block');
        });

        $('#confirm-password').on('change',function () {
            if (document.getElementById('marketplace_password').value ===
                document.getElementById('confirm-password').value) {
                document.getElementById('message').innerHTML = 'match'
            } else {
                document.getElementById('message').innerHTML = 'no match'
            }
        });


        $('#save-info').click(function (e) {
            e.preventDefault();
            saveDetails();
        });

        window.ToggleConditionalDiv = function (marketplaceId, setting) {
            if (jQuery('#setting_label_' + marketplaceId + '_' + setting).is(':checked')) {
                jQuery('#conditional_div_' + marketplaceId + '_' + setting).show()
            } else {
                jQuery('#conditional_div_' + marketplaceId + '_' + setting).hide()
            }
        }
    }

});
