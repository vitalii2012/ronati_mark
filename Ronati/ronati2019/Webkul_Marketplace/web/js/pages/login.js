define([
    "jquery",
    'mage/validation'
], function ($) {

    return function (conf){

        var loginMessage = $('#login-message');
        loginMessage.hide();

        $("#formSubmit").on("click", function () {
            var formValid = $('#login-form')
            if (formValid.validation && formValid.validation('isValid')) {
                var username = $('#email-login').val();
                var password = $('#pass').val();

                loginMessage.show();

                $.ajax({
                    url: conf.authLink,
                    showLoader: false,
                    data: {'login': username, 'password': password},
                    type: 'POST'
                }).success(function (data) {
                    let json = $.parseJSON(data);

                    if (json.data.flag == 'true') {
                        window.onbeforeunload = null;
                        jQuery('#login-message').show();
                        if (json.data.user_type == 'seller') {
                            location.href = conf.redirectLink;

                        } else {

                            window.location.href = conf.successLink;
                        }
                    } else {
                        loginMessage.show();
                        loginMessage.html('<p style=color:red;>' + json.data.message + '</p>');
                    }
                }).error(function (data) {
                    loginMessage.show();
                    loginMessage.html('<p style=color:red;>Invalid login or password</p>');
                });
            }
        });
    }

});
