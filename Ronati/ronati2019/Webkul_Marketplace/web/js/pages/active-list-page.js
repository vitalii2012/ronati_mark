define([
    'jquery',
    'mage/mage',
    'tagManager'
], function ($) {

    return function (conf) {

        $('#export_button').on("click", function () {
            $('#export_options').toggle();
        });

        $('#export_btn_options_csv').on("click", function () {
            $('#export_options').hide();
        });
        $('#export_btn_options_exel').on("click", function () {
            $('#export_options').hide();
        });
        $('#export_btn_options_pdf').on("click", function () {
            $('#export_options').hide();
        });
        $('body').on("click", function (e) {
            $('#export_options').hide();
        });

        $("#export_button_div").click(function (e) {
            e.stopPropagation();
        });

        $("#product-marketplaces-settings").submit(function (e) {

            var add_to_marketplace_popup_counter = 0;
            $(".marketplace-add").each(function () {
                if ($(this).is(':checked')) {
                    add_to_marketplace_popup_counter++;
                }
            });
            if (add_to_marketplace_popup_counter > 0) {
                $('#no-marketplace-error').hide();
            } else {
                e.preventDefault();
                $('#no-marketplace-error').show();
            }

        });

        $('#close_button_add_item, .button-close').on('click', function () {
            $('#product-marketplaces').html('');
            $('#add_item').hide();
        });


        $("#add-to-marketplace").click(function () {

            var product_ids = [];
            var str;
            var split = [];
            var counter = 0;
            var checked_products_marketplaces = [];
            var i = 1, j = 0;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    if (typeof $('#product-channel_' + i).val() !== 'undefined') {
                        var marketplace_id_array = $('#product-channel_' + i).val().split(",");
                        $.each(marketplace_id_array, function (index, value) {
                            if (typeof checked_products_marketplaces[value] === 'undefined') {
                                checked_products_marketplaces[value] = 1;
                            } else {
                                checked_products_marketplaces[value] = parseInt(checked_products_marketplaces[value]) + 1;
                            }
                        });
                    }
                    split = $(this).attr('id').split("_");
                    product_ids[j] = split[1];
                    j++;
                }
                i = i + 2;
            });
            str = product_ids.toString();
            if (counter > 0) {
                $("#market-add").fadeIn('slow').delay(5000).fadeOut('slow');
                $(window).scrollTop($('#header').offset().top);
            } else {
                $.ajax({
                    url: conf.availabeMarketPlace,
                    type: 'POST',
                    showLoader: true,
                    data: {'product_ids': str},
                    success: function (returndata) {

                        if (returndata.marketplaces) {
                            if (returndata.marketplaces.length === 0) {
                                $('.add-item-to-mp').addClass('empty-list');
                            } else {
                                $('.add-item-to-mp').removeClass('empty-list');
                            }
                            $('.success message').show();

                        } else {
                            $('.add-item-to-mp').addClass('empty-list no-mp');
                        }
                        showAddItemPopup(str, checked_products_marketplaces);
                    }
                });
            }
        });


        if ($('#empty-inventory-div').hasClass("active")) {

            $('#check-all').attr("disabled", "disabled");
        }

        $('#close_button_sold').on('click', function () {
            $('#marketplace_channel_list').html('');
            $("input[name='product-list']:checked").prop('checked', false);
            $('#mark_as_sold_popup').hide();
            return false;
        });

        //add to marketplace
        $('#add_items_with_marketplace').click(function () {
            $('.success message').hide();
            var add_to_marketplace_popup_counter = 0;
            $(".marketplace-add").each(function () {
                if ($(this).is(':checked')) {
                    add_to_marketplace_popup_counter++;
                }
            });
            if (add_to_marketplace_popup_counter > 0) {
                $('#no-marketplace-error').hide();
// Add to marketplace product
                var values = [];
                $("input[name='product-list']:checked").each(function () {
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='add_item_marketplace']:checked").each(function () {
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");
                var split;
                $.each(values, function (index, value) {
                    $(".marketplace-add").each(function () {
                        if ($(this).is(':checked')) {
                            split = $(this).attr('id').split("_");
                            $('#new-shipping-method_' + split[1]).show();
                            $('#default-check-hide-details_' + split[1]).show();
                        }
                    });
                });
                if ($('#product-marketplaces-settings').valid()) {
                    var datastring = $('#product-marketplaces-settings').serialize();
                    let data = datastring + "&productIds=" + str + "&marketplaceIds=" + marketplace_id;
                    $('#add_items_with_marketplace').text('Wait...');
                    $('#add_items_with_marketplace').attr("disabled", true);
                    $.ajax({
                        url: conf.addToMarketPlace,
                        type: 'POST',
                        data: data,
                        success: function (returndata) {
                            $('.success message').show();
                            location.reload();
                        }
                    });
                }
            } else {
                $('#no-marketplace-error').show();
            }
        });

        //remove from marketplace
        $('#remove_item_from_marketplace').click(function () {
            $('.success message').hide();
            var remove_from_marketplace_popup_counter = 0;
            $(".marketplace-remove").each(function () {
                if ($(this).is(':checked')) {
                    remove_from_marketplace_popup_counter++;
                }
            });
            if (remove_from_marketplace_popup_counter > 0) {
                $('#no-channel-error').hide();
// remove from marketplace product
                var values = [];
                $("input[name='product-list']:checked").each(function () {
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='remove_item_marketplace']:checked").each(function () {
                    marketplace.push($(this).val());
                });
                $('#remove_item_from_marketplace').text('Wait...');
                var marketplace_id = marketplace.join(",");
                $.ajax({
                    url: conf.removeFromMarketPlace,
                    type: 'POST',
                    data: {'productIds': str, 'marketplaceIds': marketplace_id},
                    success: function (returndata) {
                        $('#remove_item_from_marketplace').text('Removed...');
                        $('.success message').show();
                        location.reload();
                    }
                });

            } else {
                $('#no-channel-error').show();
            }
        });

        $('#no-thanks').on('click', function () {
            $('#disconnect').hide();
        });

        $('#close_button_remove_item,#close_window_button').on('click', function () {
            $('#remove_item').hide();
        });


        $('#close_button_add_item').on('click', function (e) {
            e.preventDefault();
            $('#add_item').hide();
            return false;
        });

        // sushil hold
        $('#close_on_hold_button').on('click', function () {
            $("input[name='product-list']:checked").prop('checked', false);
            $('#hidden_hold_popup').hide();
        });


        $('#close_button_add_item').on('click', function () {
            $('#product-marketplaces').html('');
            $('#add_item').hide();
        });


        $('#close_button_sold').on('click', function () {
            $('#mark_as_sold_popup').hide();
        });

//enable/disbale action topbar

        $('.product-list-check').click(function () {
            var counter = 0;
            $(".product-list-check").each(function () {
                if ($(this).is(':checked')) {
                    counter++;
                }
            });
            if (counter > 0) {
                $('.js-inventory-btn').attr('disabled', false);
            } else {
                $('.js-inventory-btn').attr('disabled', true);
            }
        });

        $('#check-list').click(function (event) {
            if (this.checked) {
                $(':checkbox').each(function () {
                    this.checked = true;
                    $('.js-inventory-btn').attr('disabled', false);
                });
            } else {
                $(':checkbox').each(function () {
                    this.checked = false;
                    $('.js-inventory-btn').attr('disabled', true);
                });
            }
        });

        $('#checkbox_icon').on("click", function () {
            $('#dropdown_all_check').toggle().css('visibility', 'visible');
        });

        $('#close_button').on('click', function () {
            $('#disconnect').hide();
        });

        // duplicate product product
        $('.duplicate_product').on("click", function () {
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");

            $.ajax({
                url: conf.duplicateProduct,
                type: 'POST',
                data: {'productIds': str},
                success: function (returndata) {
                    location.reload();
                }
            });
        });

        $(document).click(function () {
            $('.js-check-list').hide();
        });

        $(".js-check-list").click(function (e) {
            e.stopPropagation();
        });
        //Redirect to import screen

        $('#import-button').click(function () {
            window.location.href = conf.importProduct;
        });
    }

});
