define([
    'jquery',
    'mage/mage'
], function($) {

    return function (conf) {
        $('#export_button').on("click", function(){
            $('#export_options').toggle();
        });

        $('#export_btn_options_csv').on("click", function(){
            $('#export_options').hide();
        });
        $('#export_btn_options_exel').on("click", function(){
            $('#export_options').hide();
        });
        $('#export_btn_options_pdf').on("click", function(){
            $('#export_options').hide();
        });
        $('body').on("click", function(e){
            $('#export_options').hide();
        });

        $("#export_button_div").click(function(e) {
            e.stopPropagation();
        });

        $('#check-list').click(function(event) {
            if(this.checked) {
                // Iterate each checkbox
                $('input[name=product-list]').each(function() {
                    this.checked = true;
                    $('.js-inventory-btn').attr('disabled', false);
                });
            }
            else {
                $(':checkbox').each(function() {
                    this.checked = false;
                    $('.js-inventory-btn').attr('disabled', true);
                });
            }
        });
        // end check all checkbox of all product in product list

        $('#checkbox_icon').on("click", function(){
            //$('#dropdown_all_check').toggle();
            $('#dropdown_all_check').toggle().css('visibility','visible');
            $('#activate').hide();
        });
        //add to marketplace
        $("#add-to-marketplace").click(function(){
            var product_ids=[];
            var i=0;
            $(".product-list-check").each(function(){
                if($(this).is(':checked')){
                    product_ids[i]=$(this).val();
                    i++;
                }
            });

           var str = product_ids.toString();

            $.ajax({
                url:conf.connectProductSettings,
                type:'POST',
                data: {'productIds':str},
            cacheable:false,
                success:function(data){
                $('#product-marketplaces').append(data);
            },error:function(data){
            }

        });
        });

        if($('#empty-inventory-div').hasClass("active")){

            $('#check-all'). attr("disabled", "disabled");
        }

        $('#close_button_sold').on('click', function(){
            $('#marketplace_channel_list').html('');
            $('#delete-product-draft').hide();
            return false;
        });

        //add to marketplace
        $('#add_items_with_marketplace').click(function(){
            var add_to_marketplace_popup_counter=0;
            $(".marketplace-add").each(function() {
                if($(this).is(':checked')) {
                    add_to_marketplace_popup_counter++; }
            });
            if(add_to_marketplace_popup_counter>0){
                $('#no-marketplace-error').hide();
                // Add to marketplace product
                var values = [];
                $("input[name='product-list']:checked").each(function(){
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='add_item_marketplace']:checked").each(function(){
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");

                if($('#product-marketplaces-settings').valid()){
                    var datastring = $('#product-marketplaces-settings').serialize();
                    var data = datastring+"&productIds="+str+"&marketplaceIds="+marketplace_id;
                    $.ajax({
                        url: conf.addToMarketplace,
                        type: 'POST',
                        data: data,
                        success: function (returndata) {
                            //location.reload();
                        }
                    });
                }
            }else{$('#no-marketplace-error').show();}
        });

//remove from marketplace
        $('#remove_item_from_marketplace').click(function(){
            var remove_from_marketplace_popup_counter=0;
            $(".marketplace-remove").each(function() {
                if($(this).is(':checked')) {
                    remove_from_marketplace_popup_counter++; }
            });
            if(remove_from_marketplace_popup_counter>0){
                $('#no-channel-error').hide();
                // remove from marketplace product
                var values = [];
                $("input[name='product-list']:checked").each(function(){
                    values.push($(this).val());
                });
                var str = values.join(",");

                var marketplace = [];
                $("input[name='remove_item_marketplace_title']:checked").each(function(){
                    marketplace.push($(this).val());
                });
                var marketplace_id = marketplace.join(",");

                $.ajax({
                    url: conf.removeFromMarketplace,
                    type: 'POST',
                    data: {'productIds': str, 'marketplaceIds':marketplace_id},
                    success: function (returndata) {
                        location.reload();
                    }
                });

            }else{$('#no-channel-error').show();}
        });

        $('#no-thanks').on('click', function(){
            $('#disconnect').hide();
        });

        $('#close_button_remove_item').on('click', function(){
            $('#remove_item').hide();
        });

        $('#close_on_hold_button').on('click', function(){
            $("input[name='product-list']:checked").prop('checked',false);
            $('#hidden_hold_popup').hide();
        });


        $('#close_button_add_item').on('click', function(){
            $('#product-marketplaces').html('');
            $('#add_item').hide();
        });


        $('#close_button_sold').on('click', function(){
            $('#mark_as_sold_popup').hide();
        });

// mark as sold product
        $('#yes-delete').on("click", function(){
            var product = [];
            $("input[name='product-list']:checked").each(function(){
                product.push($(this).val());
            });
            $('#yes-delete').val("Wait..");
            $('#yes-delete').attr('disabled',true);
            $('#no-delete').attr('disabled',true);
            $.ajax({
                url: conf.permanentDelete,
                type: 'POST',
                data:{'product': product},
                success: function (returndata) {
                    location.reload();
                }
            });
        });
        $('#no-delete').on("click", function(){
            $('#marketplace_channel_list').html('');
            // $("input[name='product-list']:checked").prop('checked',false);
            $('#delete-product-draft').hide();
            return false;
        });
        // duplicate product product
        $('.duplicate_product').on("click", function(){
            var values = [];
            $("input[name='product-list']:checked").each(function(){
                values.push($(this).val());
            });

            var str = values.join(",");

            $.ajax({
                url: conf.duplicateProduct,
                type: 'POST',
                data: {'productIds': str},
                success: function (returndata) {
                    location.reload();
                }
            });
        });

        $(document).click(function() {
            $('.js-check-list').hide();
        });

        $(".js-check-list").click(function(e) {
            e.stopPropagation();
        });
        //Redirect to import screen

        $('#import-button').click(function(){
            window.location.href = conf.importProducts;
        });
    }
});
