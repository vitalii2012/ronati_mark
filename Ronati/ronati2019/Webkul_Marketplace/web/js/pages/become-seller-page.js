define([
    'jquery',
    'mage/mage'
], function ($) {

    return function (conf) {

        window.getRegionList = function(country_pic){
            $.ajax({
                url: conf.countryRegion,
                showLoader: true,
                data: {'country_id':country_pic },
                type: 'POST'
            }).done(function(data) {
                var json = $.parseJSON(data);
                if(json.length!='0') {
                    $("#seller_state").empty();
                    $.each(json, function (key, value) {
                        $("#seller_state").append($("<option value="+value.region_id+"'>"+value.default_name+"</option>"));
                    });
                    $('#seller_state_text').hide();
                } else {
                    $('#seller_state_id').hide();
                    $('#seller_state_text').show();
                }
            });
        }
    }
});

