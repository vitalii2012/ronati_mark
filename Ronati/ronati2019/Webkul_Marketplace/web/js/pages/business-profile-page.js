define([
    'jquery',
    'mage/mage',
    'typeHead',
    'tagManager'
], function ($) {

    return function (conf) {
        $("#shop_url").focus(function () {
            if($(this).val() == '' ){
                $(this).attr('value', "http://");
            }
        });

        //Auto populate function for Other/Category Tags
        var other_category_tags = $('#other_category').tagsManager({
            tagsContainer: $('#tags-options'),
            tagClass: 'tags-option',
            maxTags: 13
        });

        //  //Preselected other categories
        var DB_other_categories = conf.otherCategory;
        var arrayDB = DB_other_categories.split(",");
        $.each(arrayDB, function (i) {
            other_category_tags.tagsManager("pushTag", arrayDB[i]);
        });
        //  //End preselected Tags
        $("#other_category").blur(function () {
            other_category_tags.tagsManager("pushTag", this.value);
        });
        //
        $('#add-tag__others').on("click", function (e) {
            e.preventDefault();
            other_category_tags.tagsManager("pushTag", $("#other_category").val())
        });
        $("#other_category").typeahead({
            source: function (query, process) {
                return $.get(showFairsFile, {query: query}, function (data) {
                    return process(data);
                });
            },
            afterSelect: function (item) {
                other_category_tags.tagsManager("pushTag", item);
            }
        });
        //check if the max tags is already been hit on page load
        var other_category_tags_no = $("#other_category").tagsManager('tags');
        if (other_category_tags_no.length == 13) {
            $("#add-tag__others").hide();
        }
        //when tags reach the max limit
        $("#other_category").on('tm:hide', function (e, taglist) {
            $("#add-tag__others").hide();
        });
        //when tags can be entered again
        $("#other_category").on('tm:show', function (e, taglist) {
            $("#add-tag__others").show();
        });

        // Auto populate function for Shows/Fairs
        var show_tags = $("#shows-fairs").tagsManager({tagClass: 'tags-shows', maxTags: 13, tagsContainer: $('#Shows-div')});

        //Preselected Show Fair tags
        var DB_shows_fair_Tags = conf.partnerShowFairs;
        var arrayShowFair = DB_shows_fair_Tags.split(",");

        $.each(arrayShowFair, function (i) {
            show_tags.tagsManager("pushTag", arrayShowFair[i]);
        });
        var showFairsFile = '';
        // Auto populate function for Association/Mambership
        var association_tags = $("#member-ship").tagsManager({
            tagClass: 'tags-associations',
            maxTags: 13,
            tagsContainer: $('#Assoc-div')
        });

        //Preselected Show Fair tags
        var DB_association_tags = conf.partnerMemberShip;
        var array = DB_association_tags.split(",");
        $.each(array, function (i) {
            association_tags.tagsManager("pushTag", array[i]);
        });

        //Add remove multiple address
        var max_fields_limit_count = 10; //set limit for maximum input fields
        var x = conf.sellerAddressCount; //initialize counter for text box
        $('#add-more-address').click(function (e) { //click event on add more fields button having class add_more_button
            e.preventDefault();
            if (x < max_fields_limit_count) {
                //check conditions
                x++; //counter increment
                $('.seller-steps__location-2').append("<div id='div_" + x + "' class='add-new-location'><div class='form-field col-md-6'><label for='address' class='form-field__label'>Company Address:</label><input class='form-field__input' type='text' name='seller_address[" + x + "]' id='seller_address_" + x + "' placeholder='Enter other company address' data-validate='{required:true}' onFocus='geolocate()'><div class='default-checkbox'><input type='checkbox' id='match-retial-shop_" + x + "' name='match_retial_shop[]' class='default-checkbox__input' value='Yes'><label for='match-retial-shop_" + x + "' class='default-checkbox__label'>This is a retail shop</label></div></div><div class='form-field col-md-6'><label for='seller-choose-country' class='form-field__label'>Country:</label><select name='country_pic[" + x + "]' id='country_pic_" + x + "' data-validate='{required:true}' class='form-field__select js-select-menu' onchange='getOtherRegionList(this.value, this.id)'><option value=''>Country</option><?php  $blockObj= $block->getLayout()->createBlock('Webkul\Marketplace\Block\Account\Editprofile');$blockObj->getMyCountryMethod();foreach ($blockObj->getMyCountryMethod() as $country_value) { ?><?php if($country_value['value']!='') {?><option value='<?= $country_value[ 'value'];?>'><?= $country_value['label'];?></option><?php } } ?></select></div><div class='form-field col-md-6'><label for='address' class='form-field__label'>State/Region:</label><input id='seller_state_" + x + "' class='form-field__input' type='text' name='seller_state[" + x + "]' placeholder='Enter State/Region'/><select name='seller_state_option_manual[" + x + "]' id='seller_state_option_" + x + "' autocomplete='off' aria-required='true' class='form-field__select js-select-menu' style='display:none;'><option value=''>State</option></select></div><div class='form-field col-md-6'><label for='address' class='form-field__label'>City:</label><input id='seller_city_" + x + "' class='form-field__input' type='text' name='seller_city[" + x + "]' placeholder='Enter City'/></div><div class='form-field col-md-6'><label for='address' class='form-field__label'>Zip/Postal code:</label><input id='seller_zipcode_" + x + "' class='form-field__input' type='text' name='seller_zipcode[" + x + "]' placeholder='Enter Zip/Postal code'/><a href='javascript:void(0)' class='company-info-form__location js-add-location' onClick='removeExtraAddress(" + x + ")'>- Remove Location</a></div></div>");
                //add input field
            }
        });

        $('.seller-steps__location-2').on("click", ".remove_field", function (e) { //user click on remove text links
            e.preventDefault();
            $('#div_' + x).remove();
            x--;
        });
        //End Add remove multiple address


        //Add remove multiple inventory address change by sushil Date: 23/10/18
        var max_fields_limit_address = 10; //set limit for maximum input fields
        var z = conf.inventoryAddress; //initialize counter for text box
        $('#inventory_add-more-address').click(function (e) { //click event on add more fields button having class add_more_button
            e.preventDefault();
            if (z < max_fields_limit_address) {
                //check conditions
                z++; //counter increment
                $('.inventory_seller-steps__location-2').append("<div id='inventory_div_" + z + "' class='add-new-location'><div class='form-field col-md-6'><label for='address' class='form-field__label'>Inventory Address:</label><input class='form-field__input' type='text' name='seller_inventory_address[" + z + "]' id='inventory_seller_address_" + z + "' placeholder='Enter other company address' data-validate='{required:true}' onFocus='geolocate()'><div class='same-as-company-checkbox'><input id='same-as-company-match-retial-shop_" + z + "' type='checkbox' class='default-checkbox__input' value='' onclick='fillAddress(" + z + ")'><label for='same-as-company-match-retial-shop_" + z + "' class='default-checkbox__label'>Same as Company Address</label></div></div><div class='form-field col-md-6'><label for='seller-choose-country' class='form-field__label'>Country:</label><select name='inventory_country_pic[" + z + "]' id='inventory_country_pic_" + z + "' data-validate='{required:true}' class='form-field__select js-select-menu' onchange='getOtherRegionList(this.value, this.id)'><option value=''>Country</option><?php  $blockObj= $block->getLayout()->createBlock('Webkul\Marketplace\Block\Account\Editprofile');$blockObj->getMyCountryMethod();foreach ($blockObj->getMyCountryMethod() as $country_value) { ?><?php if($country_value[ 'value']!='') {?><option value='<?= $country_value[ 'value'];?>'><?= $country_value['label'];?></option><?php } } ?></select></div><div class='form-field col-md-6'><label for='address' class='form-field__label'>State/Region:</label><input id='inventory_seller_state_" + z + "' class='form-field__input' type='text' name='inventory_seller_state[" + z + "]' placeholder='Enter State/Region'/><select name='inventory_seller_state_option_manual[" + z + "]' id='inventory_seller_state_option_" + z + "' autocomplete='off' aria-required='true' class='form-field__select js-select-menu' style='display:none;'><option value=''>State</option></select></div><div class='form-field col-md-6'><label for='address' class='form-field__label'>City:</label><input id='inventory_seller_city_" + z + "' class='form-field__input' type='text' name='inventory_seller_city[" + z + "]' placeholder='Enter City'/></div><div class='form-field col-md-6'><label for='address' class='form-field__label'>Zip/Postal code:</label><input id='inventory_seller_zipcode_" + z + "' class='form-field__input' type='text' name='inventory_seller_zipcode[" + z + "]' placeholder='Enter Zip/Postal code'/><div class='default-inventory-checkbox'><input id='default-inventory-match-retial-shop_" + z + "' type='checkbox' name='is_default[" + z + "]' class='default-checkbox__input default-inventory' value='yes' onClick='MakeDefaultInventory(" + z + ")'><label for='default-inventory-match-retial-shop_" + z + "' class='default-checkbox__label'>This is my default inventory location</label></div><a href='javascript:void(0)' class='company-info-form__location js-add-location' onClick='removeExtraInventoryAddress(" + z + ")'>- Remove Location</a></div></div>");
                //add input field
            }
        });

        $('.inventory_seller-steps__location-2').on("click", ".inventory_remove_field", function (e) {
            e.preventDefault();
            $('#inventory_div_' + z).remove();
            z--;
        });

        $("#other").click(function(){
            if($("#other").is(':checked')){
                $('#tags-options').show();
                $("#other_category").show();
                $("#add-tag__others").show();
            }else{$("#other_category").hide();
                $('#tags-options').hide();
                $("#add-tag__others").hide();
            }
        });


        $('#save-btn').click(function() {
            var check_category= $('input[name="category_id[]"]:checked').length == 0;
            if(check_category){$('#no-cat-error').show();$(window).scrollTop($('#div-category').offset().top);}else{$('#no-cat-error').hide();}
            if($('#user-info').valid()&&(!check_category)){
                updateSellerProcess();
            }


        });

        $('#avatar').bind('change', function() {

            var filesize=this.files[0].size;
            var items = this.files[0];
            var ext = items.name.substr(-4);


            if(filesize> 1000000) {
                $('#file-size-error').html('Total Files size must not be more than 1 MB');
                $('#avatar').val('');
            } else {
                $('#file-size-error').html('');
            }

            if(ext!='.jpg' && ext!='jpeg' && ext!='.png' && ext!='.PNG' && ext!='JPEG' && ext!='.JPG')
            {
                $('#file-ext-error').html('Invalid File extention.You are allowed to upload jpeg or png files');
                $('#avatar').val('');
            } else {
                $('#file-ext-error').html('');

            }

        });
        $('#other').on('change', function () {
            let item = $('.input-wrapper');
            if($(this).prop('checked')){
                item.css('display','block');
            }else {
                item.css('display','none');
            }
        })

    }

});
