define([
    'jquery',
    'tagEditorCaret',
    'tagEditor',
], function ($) {
    'use strict';
    $.widget('ronati.customTagEditor', {

        _create: function () {
            let self = this.element;
            $(self).tagEditor();
        },
    });

    return $.ronati.customTagEditor;

});

