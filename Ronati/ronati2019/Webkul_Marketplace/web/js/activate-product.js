define([
    'jquery'
], function ($) {

    return function (conf) {
        $('.activate_product').on("click", function () {
            $('.success message').hide();
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });

            var str = values.join(",");
            $('#activate_link').text('Wait...');

            $.ajax({
                url: conf.activateProduct,
                type: 'POST',
                showLoader: true,
                data: {'productIds': str},
                success: function (returndata) {
                    $('#activate_link').text('Activated...');
                    $('.success message').show();
                    location.reload();
                }
            });
        });
    }
})
