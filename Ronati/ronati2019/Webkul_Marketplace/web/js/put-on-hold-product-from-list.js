define([
    'jquery',
    'mage/mage'
], function ($) {

    return function (conf) {

        $('#on_hold_product_inventory').on("click", function(){

            $('#on_hold_product_inventory').text('Wait...');

            var date = $('#date_on_hold').val();
            var values = [];
            $("input[name='product-list']:checked").each(function(){
                values.push($(this).val());
            });
            var str = values.join(",");
            $.ajax({
                url: conf.url,
                type: 'POST',
                data: {'productIds': str, 'date':date},
                success: function (returndata) {
                    location.reload();
                    $('.success message').show();
                }
            });
        });
    }
});
