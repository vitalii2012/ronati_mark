define([
    'jquery'
], function ($) {

    return function (conf) {
        $('.deactivate_product').on("click", function () {
            $('.success message').hide();
            var values = [];
            $("input[name='product-list']:checked").each(function () {
                values.push($(this).val());
            });
            var str = values.join(",");
            $('#deactivate_link').text('Wait...');

            $.ajax({
                url: conf.deactivateProduct,
                type: 'POST',
                showLoader: true,
                data: {'productIds': str},
                success: function (returndata) {
                    $('#deactivate_link').text('Deactivated...');
                    $('.success message').show();
                    location.reload();
                }
            });
        });
    }
});
