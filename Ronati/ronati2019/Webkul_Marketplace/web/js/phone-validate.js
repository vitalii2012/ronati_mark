define([
    'jquery',
    'intlTelInput'
], function ($, intlTelInput) {
    'use strict';
    $.widget('ronati.phoneValidation', {

        _create: function () {
            var currentEl = this.element;
            var currentInput = $('#phone-message');
            var submitButton = $('#seller-first-next-button');
            $(currentEl).on("blur", function () {
                var telInput = $(this);
                if ($.trim(telInput.val())) {
                    if (telInput.intlTelInput("isValidNumber")) {
                        currentInput.show();
                        currentInput.html("<span style='color:green;'>✓ Valid phone number</span>");
                        $('[for="contact_number"]').css('display', 'none');
                        submitButton.attr("disabled", false);
                        var nationalPhone = telInput.intlTelInput("getNumber");
                        telInput.val(nationalPhone);

                    } else {
                        currentInput.show();
                        currentInput.html("<span style='color:red;'>Invalid phone format.</span>");
                        submitButton.attr("disabled", true);
                    }
                } else {
                    currentInput.html("<span style='color:red;'>Please enter phone</span>");
                }
            });

            $(currentEl).on('keydown', function () {
                currentInput.html("");
            });
        }
    });
    return $.ronati.phoneValidation;
});
