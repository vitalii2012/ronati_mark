define([
    'jquery',
    'mage/mage'
], function ($) {

    return function (conf) {

        $('#delete_product_inventory').on("click", function () {

            $(this).addClass('disable');
            $('.success message').hide();
            $(this).text('Wait...');
            $.ajax({
                url: conf.url,
                type: 'POST',
                data: {'productIds': conf.product_id},
                success: function (returndata) {
                    $('.success message').show();
                    $('#delete_product_inventory').text('Deleted...');
                    window.location.href = conf.page;
                }
            });
        });
    }
});
