define([
    'jquery'
], function ($) {
    'use strict';

    return function () {

        var elSelelect = $('select');
        elSelelect.hover(function () {
            $(this).prop('title', $(this).find('option').filter(':selected').text());
        });
        elSelelect.on('change', function () {
            $(this).prop('title', $(this).find('option').filter(':selected').text());
        });

    }
});
