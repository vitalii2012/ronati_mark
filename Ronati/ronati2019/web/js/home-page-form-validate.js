require([
    'jquery',
    'mage/url',
    'mage/mage'
], function ($, url) {

    var dataContactForm = $('#contact-form');
    dataContactForm.mage('validation', {});

    $("#contact-form-submit").click(function () {
        $('#second-contact-section').text("");

        var contact_email_address = $('#contact-email').val();
        var fullname = $('#contact-name').val();
        var contact_message = $('#contact_message').val();

        if ($('#contact-form').valid()) {

            $.ajax({
                url: url.build('auth/contactus'),
                showLoader: false,
                data: {'contact_email_address': contact_email_address, 'fullname': fullname, 'contact_message': contact_message},
                type: 'POST'
            }).success(function (data) {
                var json = $.parseJSON(data);

                $('#confirmation-message').html("<br /n>Thanks for reaching out!<br /n><br /n>");
                $('#confirmation-message2').html("<br /n>We'll get back to you shortly.<br /n><br /n><br /n>");

                $('#contact-us-form').hide();
                $('#second-contact-section').text("Thanks for contacting with us.");
            });
        }
    });
});
