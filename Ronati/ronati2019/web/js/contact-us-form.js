define([
    'jquery',
    'mage/url',
    'mage/mage'
], function ($, url) {
    var dataContactForm = $('#contact-form');
    dataContactForm.mage('validation', {});

    $("#contact-btn").click(function () {

        var contact_email_address = $('#contact_email_address').val();
        var fullname = $('#fullname').val();
        var contact_message = $('#contact_message').val();

        if ($('#contact-form').valid()) {
            $('#screen-error-message').show();
            $('#screen-error-message').html("<span style='color:#c9a77a;'>Sending your query...</span>");

            $.ajax({
                url: url.build('auth/contactus'),
                showLoader: false,
                data: {'contact_email_address': contact_email_address, 'fullname': fullname, 'contact_message': contact_message},
                type: 'POST'
            }).success(function (data) {
                //var json = $.parseJSON(data);
                $('#screen-error-message').html("<span style='color:#c9a77a;'>Thanks for contacting with us.</span>");
                $('#contact_email_address').val('');
                $('#fullname').val('');
                $('#contact_message').val('');
            });
        }
    });
});
