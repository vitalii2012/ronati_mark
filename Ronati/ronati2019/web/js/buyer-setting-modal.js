require(
    [
        'jquery',
        'Magento_Ui/js/modal/modal'
    ],
    function($,modal){
        $("#tab_a_content").on("click",function(){
            $('#tab_b').hide();
            $('#tab_a').show();
            $('#tab_c').hide();
            $('#tab_d').hide();

            $('#tab_a_content').addClass('active');
            $('#tab_b_content').removeClass('active');
            $('#tab_c_content').removeClass('active');
            $('#tab_d_content').removeClass('active');
            return false;
        });
        $("#tab_b_content").on("click",function(){
            $('#tab_b').show();
            $('#tab_a').hide();
            $('#tab_c').hide();
            $('#tab_d').hide();

            $('#tab_a_content').removeClass('active');
            $('#tab_b_content').addClass('active');
            $('#tab_c_content').removeClass('active');
            $('#tab_d_content').removeClass('active');
            return false;
        });
        $("#tab_c_content").on("click",function(){
            $('#tab_b').hide();
            $('#tab_a').hide();
            $('#tab_c').show();
            $('#tab_d').hide();

            $('#tab_a_content').removeClass('active');
            $('#tab_b_content').removeClass('active');
            $('#tab_c_content').addClass('active');
            $('#tab_d_content').removeClass('active');
            return false;
        });
        $("#tab_d_content").on("click",function(){
            $('#tab_b').hide();
            $('#tab_a').hide();
            $('#tab_c').hide();
            $('#tab_d').show();

            $('#tab_a_content').removeClass('active');
            $('#tab_b_content').removeClass('active');
            $('#tab_c_content').removeClass('active');
            $('#tab_d_content').addClass('active');
            return false;
        });
    }

);
