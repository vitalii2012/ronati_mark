(function ($) {

    var blank = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";

    var imageCenter = function (obj, options) {
        var defaults = {'forceSmart': true};

        function forceWidth(obj) {
            obj.css('width', '100%');
        }

        function forceHeight(obj) {
            obj.css('height', '100%');
        }

        function mainResize(img) {

            var _conwidth = img.parent().width(),
                _conheight = img.parent().height(),
                _parentpos = img.parent().css('position');

            img.css('width', 'auto');
            img.css('height', 'auto');

            var _fullratio = img.width() / img.height(),
                _conratio = _conwidth / _conheight;

            if (_fullratio < _conratio) {
                forceWidth(img);
            } else{
                forceHeight(img);
            }

            var _finalwidth = img.width(), _finalheight = img.height();

            img.css({
                'position': 'relative',
                'left': -(_finalwidth - _conwidth) / 2 + 'px',
                'top': -(_finalheight - _conheight) / 2 + 'px'
            }).addClass('done').parent().css({
                'position': _parentpos,
                'overflow': 'hidden'
            });
        }

        var img = $(obj), settings = $.extend(defaults, options);

        mainResize(img);

        img.load(function () {
            mainResize(img);
        });

        var resDeb = debounce(function () {
            mainResize(img);
        }, 200);

        window.addEventListener('resize', resDeb);

        if (obj.complete || obj.complete === undefined) {
            var src = obj.src;
            obj.src = blank;
            obj.src = src;
        }
    };

    $.fn.imageCenter = function (options) {

        return this.each(function (e) {
            var img = $(this);

            if (img.data('imageCenter'))
                return;

            if(img.is(':visible')) {
                var imgcenter = new imageCenter(this, options);
                img.data('imageCenter', imgcenter);
            }

        });

    }

})(jQuery);