define([
    'jquery'
], function ($) {
    'use strict';
    $.widget('ronati.floatButton', {

        _create: function () {
            $(window).on('scroll', this._buttonVisibility);
            $(document).ready(this._buttonVisibility)
        },

        _buttonVisibility  : function () {
            let windowHeight = $(window).outerHeight();
            let pageHeight = $('body').height();
            let footerHeight = $('#ronati-footer').outerHeight();
            let spaceHeight = windowHeight + footerHeight;
            if ($(this).scrollTop() > (pageHeight - spaceHeight - 250)) {
                $('.pos-fixed').fadeOut();
            } else {

                $('.pos-fixed').fadeIn();
            }
        }
    });
    return $.ronati.floatButton;
});

