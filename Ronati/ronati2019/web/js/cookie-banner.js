define([
    "jquery",
    "jquery/jquery.cookie"
], function ($) {

    //Cookie accept by user
    var check_cookie = $.cookie('cookievalue'), // Get Cookie Value
        cookieBannerContainer = $('.cookies-text-wrapper'),
        cookieBanner = $('.cookies-text');

    if (check_cookie != null) {
        cookieBannerContainer.removeClass('visible');
    } else {
        cookieBannerContainer.addClass('visible');
        changeHeight();
        $(window).on('resize', changeHeight);
    }

    $('#accept').click(function () {
        check_cookie = $.cookie('cookievalue', 1);
        cookieBannerContainer.removeClass('visible');
        changeHeight();
    });

    function changeHeight() {
        if (cookieBannerContainer.hasClass('visible')) {
            var bannerHeight = cookieBanner.outerHeight();

            cookieBannerContainer.css({paddingBottom: bannerHeight + 'px'});
        } else {
            cookieBannerContainer.css({paddingBottom: 0});
            $(window).off('resize', changeHeight);
        }
    }
});
