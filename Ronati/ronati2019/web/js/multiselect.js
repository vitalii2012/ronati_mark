define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('ronati.multiselect', {
        options: {
            selectedClass: 'ronati-multiselect__option_selected',
            wrapperClass: 'ronati-multiselect',
            wrapperFocusedClass: 'ronati-multiselect_focused',
            optionClass: 'ronati-multiselect__option'
        },

        _create: function () {
            var self = this;

            self.createMultiselect(self, this.element);
            // self.createEvents(self, this.element);
        },

        createMultiselect: function (widget, element) {
            var html = '<ul tabindex="1" class="' + widget.options.wrapperClass + '">'
            $(element).find('option').each(function () {
                var item = $(this);

                if (item.prop('selected')) {
                    html += '<li class="' + widget.options.optionClass + ' ' + widget.options.selectedClass +
                        '" data-value="' + item.prop('value') + '">' + item.text() + '</li>'
                } else {
                    html += '<li class="' + widget.options.optionClass + '" data-value="' +
                        item.prop('value') + '">' + item.text() + '</li>'
                }
            });

            html += '</ul>';
            var customMultiselect = $(html).insertAfter(element);

            widget.createEvents(widget, element, customMultiselect)
        },

        createEvents: function (self, defaultElement, customElement) {
            customElement.find('.' + self.options.optionClass).on('click', function (e) {
                var targetOption = $(e.target),
                    parentOption = defaultElement.find('[value="' + targetOption.data("value") + '"]');

                if (targetOption.hasClass(self.options.selectedClass)) {
                    parentOption.prop('selected', false);
                } else {
                    parentOption.prop('selected', true);
                }

                defaultElement.trigger('change');
                targetOption.toggleClass(self.options.selectedClass);
            });

            defaultElement.on('focus', function () {
                customElement.trigger('focus');
            });
        }
    });

    return $.ronati.multiselect;
});
