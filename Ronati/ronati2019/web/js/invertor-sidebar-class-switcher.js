require([
    'jquery',
    'mage/mage'
], function($) {
    function getUrl() {
        var currentLink = window.location.pathname;
        var currentPage = currentLink.substring(21,currentLink.length -1);
        if(!currentPage.includes("/")){
            $('#'+ currentPage).addClass('active');
        }
    }

    getUrl();

});
