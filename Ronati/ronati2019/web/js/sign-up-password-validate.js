require([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function($){
    $('#seller_password').attr('data-validate', $('#seller_password').attr('data-validate').replace('validate-customer-password','validate-customer-password-custom'));
    $.validator.addMethod(
        'validate-customer-password-custom', function(v, elm) {
            var validator = this,
                length = 0,
                counter = 0;
            var passwordMinLength = $(elm).data('password-min-length');
            var passwordMinCharacterSets = $(elm).data('password-min-character-sets');
            var pass = $.trim(v);
            var result = pass.length >= passwordMinLength;
            if (result == false) {
                validator.passwordErrorMessage = $.mage.__(
                    "Must be at least 8 characters long and contain at least 3 different types: Lower Case, Upper Case, Numbers, and/or Special Characters."
                ).replace('%1', passwordMinLength);
                return result;
            }
            if (pass.match(/\d+/)) {
                counter ++;
            }
            if (pass.match(/[a-z]+/)) {
                counter ++;
            }
            if (pass.match(/[A-Z]+/)) {
                counter ++;
            }
            if (pass.match(/[^a-zA-Z0-9]+/)) {
                counter ++;
            }
            if (counter < passwordMinCharacterSets) {
                result = false;
                validator.passwordErrorMessage = $.mage.__(
                    "Must be at least 8 characters long and contain at least 3 different types: Lower Case, Upper Case, Numbers, and/or Special Characters."
                ).replace('%1', passwordMinCharacterSets);
            }
            return result;
        }, function () {
            return this.passwordErrorMessage;
        });
});
