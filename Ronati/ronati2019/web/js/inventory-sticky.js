define([
    'jquery',
    'stickyKit'
], function ($, intlTelInput) {
    'use strict';
    $.widget('ronati.sticky', {

        _create: function () {
            var self = this,
                element = self.element,
                bannerHeight = $('.cookies-text-wrapper').outerHeight();
            $(element).stick_in_parent({
                recalc_every: 15,
                offset_top: bannerHeight
            }).on("sticky_kit:stick", function(e) {
                $(e.target).addClass('sticky-panel');
            }).on("sticky_kit:unstick", function(e) {
                $(e.target).removeClass('sticky-panel');
            });

            $(window).on('resize', function (e) {
                var newWidth = 0;
                if($('.cookies-text-wrapper').outerHeight()) {
                    newWidth = $('.cookies-text').outerHeight();
                }

                $(element).trigger("sticky_kit:detach");
                $(element).stick_in_parent({
                    recalc_every: 15,
                    offset_top: newWidth
                })
            });

            $('#accept').on('click', function () {
                $(element).trigger("sticky_kit:detach");
                $(element).stick_in_parent({
                    recalc_every: 15,
                    offset_top: 0
                });
            })
        }
    });
    return $.ronati.sticky;

});
